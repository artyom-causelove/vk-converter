import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './_reducers';
import { currencyConstants } from './_constants';

export const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware
  )
);

store.dispatch({ type: currencyConstants.LOAD_FROM_HISTORY });

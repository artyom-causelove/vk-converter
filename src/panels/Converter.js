import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Panel, PanelHeader, FormLayout, Select, Spinner, Input, Button, Div, List, Cell } from '@vkontakte/vkui';
import { connect } from 'react-redux';

import { currencyService } from '../_services/currency.service';
import { currencyActions } from '../_actions';
import '../styles.css';
import './Converter.css';

const Converter = ({ id, loading, rates, history, getRates, addToHistory }) => {
  const [sourceCurrency, setSourceCurrency] = useState(null);
  const [targetCurrency, setTargetCurrency] = useState(null);
  const [count, setCount] = useState(0);
  const [result, setResult] = useState(0);

  const onSourceChange = e => {
    const value = e.currentTarget.value;
    setSourceCurrency(value);
    onCurrencyChange(value, targetCurrency);
  };

  const onTargetChange = e => {
    const value = e.currentTarget.value;
    setTargetCurrency(e.currentTarget.value);
    onCurrencyChange(sourceCurrency, value);
  };

  const onCurrencyChange = async (sourceCurrency, targetCurrency) => {
    if (sourceCurrency && targetCurrency) {
      const rates = await getRates(sourceCurrency, targetCurrency);
      setResult((count !== '' ? count : 0) * rates);
    }
  };

  const onCountInput = e => {
    const value = e.currentTarget.value;
    setCount(value);
    if (rates) {
      setResult((value !== '' ? value : 0) * rates);
    }
  };

  const onResultInput = e => {
    const value = e.currentTarget.value;
    setResult(value);
    if (rates) {
      setCount((value !== '' ? value : 0) / rates);
    }
  };

  const onSave = () => {
    addToHistory(sourceCurrency, count, targetCurrency, result);
  };

  return (
    <Panel id={id}>
      <PanelHeader>Конвертер</PanelHeader>
      <div className={'spinner-wrapper' + (loading ? ' active' : '')}>
        <Spinner size="large"/>
      </div>
      <FormLayout>
        <Select
          className="text--primary text--bold"
          top="Конвертировать из"
          placeholder="Выберите валюту"
          onChange={onSourceChange}
        >
          {currencyService.codes.map(code => 
            <option key={code} value={code}>{code}</option>
          )}
        </Select>
        <Input
          className="text--primary text--bold"
          top="Количество"
          type="number"
          placeholder="Введите количество"
          onInput={onCountInput}
          onChange={onCountInput}
          value={count}
        />
        <Select 
          className="text--primary text--bold"
          top="Конвертировать в"
          placeholder="Выберите валюту"
          onChange={onTargetChange}
        >
          {currencyService.codes.map(code => 
            <option key={code} value={code}>{code}</option>
          )}
        </Select>
        <Input
          className="text--primary text--bold"
          top="Результат"
          type="number"
          onInput={onResultInput}
          onChange={onResultInput}
          value={result}
        />
        <Div className="flex">
          <Button onClick={onSave} disabled={!sourceCurrency || !targetCurrency} size="l" stretched>Сохранить</Button>
        </Div>
        <List top="История переводов">
          {(history || []).reverse().map((value, i) =>
            <Cell key={i} className="text--primary text--semi-bold">{value}</Cell>
          )}
        </List>
      </FormLayout>
    </Panel>
  );
};

Converter.propTypes = {
	id: PropTypes.string.isRequired
};

function mapState(state) {
  const { currency } = state;
  const { loading, rates, history } = currency;
  return { loading, rates, history };
};

const actionCreators = {
  getRates: currencyActions.getRates,
  addToHistory: currencyActions.addToHistory
};

const connectedConverter = connect(mapState, actionCreators)(Converter);
export { connectedConverter as Converter };

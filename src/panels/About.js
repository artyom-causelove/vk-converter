import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Panel, PanelHeader, Button } from '@vkontakte/vkui';
import bridge from '@vkontakte/vk-bridge';

import '../styles.css';
import './About.css';
import banner1 from '../img/banner1.png';
import banner2 from '../img/banner2.png';
import man1 from '../img/man1.png';
import man2 from '../img/man2.png';
import man3 from '../img/man3.png';
import man4 from '../img/man4.png';
import man5 from '../img/man5.png';
import man6 from '../img/man6.png';
import aquila from '../img/aquila.png';
import ava from '../img/iam.jpg';

const About = ({ id }) => {
  useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
  }, []);
  
  const onSubscribe = () => {
    bridge.send('VKWebAppJoinGroup', {'group_id': 199151576 });
  };

  const onRepost = () => {
    bridge.send('VKWebAppShowWallPostBox', {'message': 'Мое тестовое VK Mini Apps приложение: https://vk.com/app7615564_87831430'});
  };

  return (
    <Panel className="panel" id={id}>
      <PanelHeader>Об авторе</PanelHeader>
      <img className="avatar" src={ava} alt="ava"/>
      <img className="banner" src={banner1} alt="banner1"/>
      <img className="banner" src={banner2} alt="banner1"/>
      <img className="man" src={man1} alt="man1"/>
      <img className="man" src={man2} alt="man2"/>
      <img className="man" src={man3} alt="man3"/>
      <img className="man" src={man4} alt="man4"/>
      <img className="man" src={man5} alt="man5"/>
      <img className="man" src={man6} alt="man6"/>
      <img className="aquila" src={aquila} alt="aquila"/>
      <p className="name">Artyom Kozlov</p>
      <div className="wrapper flex">
        <Button onClick={onRepost} className="button" size="l" stretched>Репост</Button>
        <Button onClick={onSubscribe} className="button" size="l" stretched>Aliquam!</Button>
      </div>
    </Panel>
  );
};

About.propTypes = {
	id: PropTypes.string.isRequired
};

export { About };

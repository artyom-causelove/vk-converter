import { currencyConstants } from '../_constants';
import { currencyService } from '../_services/currency.service';

export const currencyActions = {
  getRates,
  addToHistory,
  loadFromHistory
};

function getRates(base, target) {
  return dispatch => {
    dispatch(request());

    return currencyService.getRates(base, target)
      .then(rates => { dispatch(success(rates)); return rates; });
  };

  function request() { return { type: currencyConstants.GET_RATES_REQUEST } };
  function success(rates) { return { type: currencyConstants.GET_RATES_SUCCESS, rates } };
}

function addToHistory(base, count, target, result) {
  return dispatch => {
    dispatch(success(`${count} ${base} -> ${result} ${target}`));
  };

  function success(string) { return { type: currencyConstants.ADD_TO_HISTORY, string } };
}

function loadFromHistory() {
  return dispatch => {
    dispatch(success());
  };

  function success() { return { type: currencyConstants.LOAD_FROM_HISTORY } };
}

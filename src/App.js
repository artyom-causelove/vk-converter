import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import '@vkontakte/vkui/dist/vkui.css';

import Icon28SlidersOutline from '@vkontakte/icons/dist/28/sliders_outline';
import Icon28FavoriteOutline from '@vkontakte/icons/dist/28/favorite_outline';
import { Epic, View, Tabbar, TabbarItem } from '@vkontakte/vkui';
import { Converter } from './panels/Converter';
import { About } from './panels/About';

import './styles.css';
import './App.css';

const App = () => {
	const [activeStory] = useState('default');
	const [activePanel, setActivePanel] = useState('converter');

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
	}, []);

	const onStoryChange = e => {
		setActivePanel(e.currentTarget.dataset.story);
	};

	return (
		<Epic activeStory={activeStory} tabbar={
			<Tabbar itemsLayout="vertical">
				<TabbarItem
					onClick={onStoryChange}
					selected={activeStory === 'converter'}
					data-story="converter"
					text="Конвертер"
					className="text--primary text--bold"
				><Icon28SlidersOutline /></TabbarItem>
				<TabbarItem
					onClick={onStoryChange}
					selected={activeStory === 'about'}
					data-story="about"
					text="Об авторе"
					className="text--primary text--bold"
				><Icon28FavoriteOutline/></TabbarItem>
			</Tabbar>
		}>
			<View id="default" activePanel={activePanel}>
				<Converter id="converter"></Converter>
				<About id="about"></About>
			</View>
		</Epic>
	);
}

export default App;


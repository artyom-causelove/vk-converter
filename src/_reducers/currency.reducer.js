import { currencyConstants } from '../_constants';

export function currency(state = {}, action) {
  switch (action.type) {
    case currencyConstants.GET_RATES_REQUEST:
      return {
        ...state,
        loading: true
      };
    case currencyConstants.GET_RATES_SUCCESS:
      return {
        ...state,
        loading: false,
        rates: action.rates
      };
    case currencyConstants.ADD_TO_HISTORY:
      const newHistory = state.history.concat(action.string);
      localStorage.setItem('history', JSON.stringify(newHistory));
      return {
        ...state,
        history: newHistory
      };
    case currencyConstants.LOAD_FROM_HISTORY:
      return {
        ...state,
        history: JSON.parse(localStorage.getItem('history')) || []
      };
    default:
      return state;
  }
}
